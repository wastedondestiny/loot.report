import fetch from 'node-fetch'
import { existsSync } from 'node:fs'
import { writeFile, rm, mkdir } from 'node:fs/promises'
import { exoticActivities, otherExotics } from './releaseDetail'

type ManifestResponse = {
  Response: {
    version: string,
    jsonWorldComponentContentPaths: {
      en: {
        DestinyInventoryItemLiteDefinition: string,
        DestinyCollectibleDefinition: string
      }
    }
  }
}

type CollectibleDefinition = Record<number, {
  hash: number,
  itemHash: number,
  displayProperties: {
    name: string,
    icon: string,
    description: string
  }
}>

type ItemDefinition = Record<number, {
  displayProperties: {
    name: string,
    icon: string
  },
  itemTypeAndTierDisplayName: string,
  iconWatermark: string
}>

const noop = () => {
  // No operation
}

const getManifest = async () => {
  const manifestUrl = 'https://www.bungie.net/Platform/Destiny2/Manifest/'
  const manifestResult = await fetch(manifestUrl)
  const manifestData = await manifestResult.json() as ManifestResponse

  console.log(`Loaded manifest ${manifestData.Response.version}`)

  return manifestData
}

const createFolders = async () => {
  if (existsSync('./src/generated')) await rm('./src/generated', { recursive: true })
  await mkdir('./src/generated')

  if (existsSync('./public/generated')) await rm('./public/generated', { recursive: true })
  await mkdir('./public/generated')
}

const generateCollectibles = async (manifestData: ManifestResponse) => {
  console.log(`Generating collectibles from ${manifestData.Response.version}`)

  const collectibleList = [...exoticActivities.map(x => x.collectible), ...otherExotics.map(x => x.collectible)]

  const collectibleDefinitionUrl = `https://www.bungie.net${manifestData.Response.jsonWorldComponentContentPaths.en.DestinyCollectibleDefinition}`
  const collectibleDefinitionResult = await fetch(collectibleDefinitionUrl)
  const collectibleDefinitionData: CollectibleDefinition = await collectibleDefinitionResult.json() as never

  const itemDefinitionUrl = `https://www.bungie.net${manifestData.Response.jsonWorldComponentContentPaths.en.DestinyInventoryItemLiteDefinition}`
  const itemDefinitionResult = await fetch(itemDefinitionUrl)
  const itemDefinitionData: ItemDefinition = await itemDefinitionResult.json() as never

  const items = collectibleList.map(x => collectibleDefinitionData[x].itemHash)
  const imagePaths = [...new Set(items.flatMap(x => [itemDefinitionData[x]?.displayProperties?.icon, itemDefinitionData[x]?.iconWatermark]))]

  const collectibles = collectibleList.map(x => {
    const collectible = collectibleDefinitionData[x]
    const item = itemDefinitionData[collectible.itemHash]
    return {
      id: collectible.hash,
      name: item?.displayProperties?.name,
      icon: item?.displayProperties?.icon?.split(/(\\|\/)/g).pop(),
      type: item?.itemTypeAndTierDisplayName,
      watermark: item?.iconWatermark?.split(/(\\|\/)/g).pop()
    }
  })

  await writeFile('./src/generated/collectibleDefinitions.json', JSON.stringify(collectibles))
  await Promise.all(imagePaths.map(x => fetch(`https://bungie.net${x}`).then(y => y.buffer()).then(y => writeFile(`./public/generated/${x.split(/(\\|\/)/g).pop()}`, y)).catch(noop)))

  console.log(`Generated ${collectibles.length} collectibles!`)
}

const generate = async () => {
  const [manifestData] = await Promise.all([
    getManifest(),
    createFolders()
  ])
  await Promise.all([
    generateCollectibles(manifestData)
  ])
}

generate()
