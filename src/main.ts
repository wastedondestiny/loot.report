import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './routes'
import Tracker from '@openreplay/tracker'
import * as Sentry from '@sentry/vue'

try {
  const tracker = new Tracker({
    projectKey: 'rVwRDySxb6Kgko42eysM',
    ingestPoint: 'https://mon.binar.ca/ingest',
  })
  tracker?.start()
} catch (_) {
  // noop
}

window.googleAdsClient = import.meta.env.VITE_GOOGLE_ADS_CLIENT
window.googleAdsSlot = import.meta.env.VITE_GOOGLE_ADS_SLOT

const app = createApp(App)

Sentry.init({
  app,
  dsn: 'https://943a314c2b7045278b0a2ba62aa2387b@log.binar.ca/6',
  integrations: [
    Sentry.browserTracingIntegration({ router }),
    Sentry.replayIntegration()
  ],
  tracesSampleRate: 1.0,
  replaysSessionSampleRate: 0.1,
  replaysOnErrorSampleRate: 1.0,
})

app.use(router)
app.mount('#app')

console.log('%cloot.report loaded!', 'color:yellow;-webkit-text-stroke:1px black;font-size:36px;')
setTimeout(() => console.log('You wouldn\'t paste code a stranger online gave you into this console, would you?'), 1000)
