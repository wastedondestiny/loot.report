import { ExoticActivity, ActivityId, ActivityType } from './enums/activities'
import { Expansions } from './enums/expansions'
import { BeyondLightSeasonHashes, FinalShapeEpisodeHashes, LightfallSeasonHashes, WitchQueenSeasonHashes } from './enums/seasons'
import { FinalShapeDungeonHashes, LightfallDungeonHashes, WitchQueenDungeonHashes } from './enums/dungeons'
import { ExoticElement, ExoticSourceExpansion, ExoticSourceType, ExoticType, ExoticWeapon } from './enums/releases'

export const exoticActivities: ExoticActivity[] = [
  {
    name: 'Last Wish',
    id: ActivityId.LastWish,
    type: ActivityType.Raid,
    date: new Date('2018-09-14 17:00:00 UTC'),
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `The One Thousand Voices exotic fusion rifle is a random drop from the Last Wish raid. It requires the Forsaken Pack to acquire.`,
    collectible: 199171385,
    isGuaranteed: false,
    allActivityHashes: [1661734046, 2122313384, 2214608156, 2214608157]
  },
  {
    name: 'Shattered Throne',
    id: ActivityId.ShatteredThrone,
    type: ActivityType.Dungeon,
    date: new Date('2018-09-25 17:00:00 UTC'),
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `The Wish-Ender exotic bow is a guaranteed reward from a quest inside the Shattered Throne dungeon. It requires the Forsaken pack to acquire.`,
    collectible: 1660030044,
    isGuaranteed: true,
    allActivityHashes: [2032534090]
  },
  {
    name: 'Garden of Salvation',
    id: ActivityId.GardenOfSalvation,
    type: ActivityType.Raid,
    date: new Date('2019-10-05 17:00:00 UTC'),
    releasedWith: Expansions.Shadowkeep,
    ownership: [Expansions.Shadowkeep],
    description: `The Divinity exotic trace rifle is a guaranteed reward from a quest inside the Garden of Salvation raid. It requires the Shadowkeep pack to acquire.`,
    collectible: 1988948484,
    isGuaranteed: true,
    allActivityHashes: [1042180643, 2497200493, 2659723068, 3458480158, 3845997235]
  },
  {
    name: 'Pit of Heresy',
    id: ActivityId.PitOfHeresy,
    type: ActivityType.Dungeon,
    date: new Date('2019-10-29 17:00:00 UTC'),
    releasedWith: Expansions.Shadowkeep,
    ownership: [Expansions.Shadowkeep],
    description: `The Xenophage exotic machine gun is a guaranteed reward from a quest inside the Pit of Heresy dungeon. It requires the Shadowkeep pack to acquire.`,
    collectible: 1258579677,
    isGuaranteed: true,
    allActivityHashes: [785700673, 785700678, 1375089621, 2559374368, 2559374374, 2559374375, 2582501063]
  },
  {
    name: 'Deep Stone Crypt',
    id: ActivityId.DeepStoneCrypt,
    type: ActivityType.Raid,
    date: new Date('2020-11-21 17:00:00 UTC'),
    releasedWith: Expansions.BeyondLight,
    ownership: [Expansions.BeyondLight],
    description: `The Eyes of Tomorrow exotic rocket launcher is a random drop from the Deep Stone Crypt raid. It requires the Beyond Light pack to acquire.`,
    collectible: 753200559,
    isGuaranteed: false,
    allActivityHashes: [910380154, 3976949817]
  },
  {
    name: 'Presage',
    id: ActivityId.Presage,
    type: ActivityType.ExoticMission,
    date: new Date('2021-02-16 17:00:00 UTC'),
    releasedWith: BeyondLightSeasonHashes.Chosen,
    ownership: [BeyondLightSeasonHashes.Chosen, Expansions.BeyondLight],
    description: `The Dead Man's Tale exotic scout rifle is a guaranteed drop from the Presage exotic mission. It requires the Beyond Light pack or the Season of the Chosen to acquire.`,
    collectible: 3324472233,
    isGuaranteed: true,
    allActivityHashes: [2124066889, 3883295757, 4201846671, 4212753278]
  },
  {
    name: 'Vault of Glass',
    id: ActivityId.VaultOfGlass,
    type: ActivityType.Raid,
    date: new Date('2021-05-22 17:00:00 UTC'),
    releasedWith: BeyondLightSeasonHashes.Splicer,
    ownership: [],
    description: `The Vex Mythoclast exotic fusion rifle is a random drop from the Vault of Glass raid. It is available to all players.`,
    collectible: 2300465938,
    isGuaranteed: false,
    allActivityHashes: [1485585878, 1681562271, 3022541210, 3711931140, 3881495763]
  },
  {
    name: 'Grasp of Avarice',
    id: ActivityId.GraspOfAvarice,
    type: ActivityType.Dungeon,
    date: new Date('2021-12-07 17:00:00 UTC'),
    releasedWith: Expansions.Anniversary30th,
    ownership: [Expansions.Anniversary30th],
    description: `The Gjallarhorn exotic rocket launcher is a guaranteed reward from a quest inside the Grasp of Avarice dungeon. it requires the 30th Anniversary pack to acquire.`,
    collectible: 4027219968,
    isGuaranteed: true,
    allActivityHashes: [1112917203, 3774021532, 4078656646]
  },
  {
    name: 'Vox Obscura',
    id: ActivityId.VoxObscura,
    type: ActivityType.ExoticMission,
    date: new Date('2022-02-22 17:00:00 UTC'),
    releasedWith: WitchQueenSeasonHashes.Risen,
    ownership: [WitchQueenSeasonHashes.Risen, Expansions.TheWitchQueen],
    description: `The Dead Messenger exotic grenade launcher is a guaranteed drop from the Vox Obscura exotic mission. It requires the Witch Queen expansion or the Season of the Risen to acquire.`,
    collectible: 360554695,
    isGuaranteed: true,
    allActivityHashes: [613120446, 666172264, 901429423, 2668737148]
  },
  {
    name: 'Vow of the Disciple',
    id: ActivityId.VowOfTheDisciple,
    type: ActivityType.Raid,
    date: new Date('2022-03-05 17:00:00 UTC'),
    releasedWith: Expansions.TheWitchQueen,
    ownership: [Expansions.TheWitchQueen],
    description: `The Collective Obligation exotic pulse rifle is a random drop from the Vow of the Disciple raid. it requires the Witch Queen expansion to acquire.`,
    collectible: 2817568609,
    isGuaranteed: false,
    allActivityHashes: [1441982566, 2906950631, 3889634515, 4156879541, 4217492330]
  },
  {
    name: 'Duality',
    id: ActivityId.Duality,
    type: ActivityType.Dungeon,
    date: new Date('2022-05-27 17:00:00 UTC'),
    releasedWith: WitchQueenSeasonHashes.Haunted,
    ownership: [WitchQueenDungeonHashes.Duality],
    description: `The Heartshadow exotic sword is a random drop from the Duality dungeon. It requires the Witch Queen Dungeon Key to acquire.`,
    collectible: 467760883,
    isGuaranteed: false,
    allActivityHashes: [1668217731, 2823159265, 3012587626]
  },
  {
    name: 'King\'s Fall',
    id: ActivityId.KingsFall,
    type: ActivityType.Raid,
    date: new Date('2022-08-26 17:00:00 UTC'),
    releasedWith: WitchQueenSeasonHashes.Plunder,
    ownership: [],
    description: `The Touch of Malice exotic scout rifle is a random drop from the King's Fall raid. It is available to all players.`,
    collectible: 192937277,
    isGuaranteed: false,
    allActivityHashes: [1063970578, 1374392663, 2897223272, 2964135793, 3257594522]
  },
  {
    name: 'Spire of the Watcher',
    id: ActivityId.SpireOfTheWatcher,
    type: ActivityType.Dungeon,
    date: new Date('2022-12-09 17:00:00 UTC'),
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [WitchQueenDungeonHashes.SpireOfTheWatcher],
    description: `The Hierarchy of Needs exotic bow is a random drop from the Spire of the Watcher dungeon. It requires the Witch Queen Dungeon Key to acquire.`,
    collectible: 3558330464,
    isGuaranteed: false,
    allActivityHashes: [1262462921, 1801496203, 2296818662]
  },
  {
    name: 'Operation: Seraph\'s Shield',
    id: ActivityId.OperationSeraphShield,
    type: ActivityType.ExoticMission,
    date: new Date('2022-12-20 17:00:00 UTC'),
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [WitchQueenSeasonHashes.Seraph, Expansions.TheWitchQueen],
    description: `The Revision Zero exotic pulse rifle is a guaranteed drop from the Operation: Seraph's Shield exotic mission. It requires the Witch Queen expansion or the Season of the Seraph to acquire.`,
    collectible: 1161231112,
    isGuaranteed: true,
    allActivityHashes: [202306511, 995051012, 1221538367, 2919809209]
  },
  {
    name: '//node.ovrd.AVALON//',
    id: ActivityId.NodeOvrdAvalon,
    type: ActivityType.ExoticMission,
    date: new Date('2023-03-07 17:00:00 UTC'),
    releasedWith: LightfallSeasonHashes.Defiance,
    ownership: [LightfallSeasonHashes.Defiance],
    description: `The Vexcalibur exotic glaive is a guaranteed drop from the //node.ovrd.AVALON// exotic mission. It requires the Lightfall expansion or the Season of Defiance to acquire.`,
    collectible: 2629609052,
    isGuaranteed: true,
    allActivityHashes: [3083261666, 3755529435]
  },
  {
    name: 'Root of Nightmares',
    id: ActivityId.RootOfNightmares,
    type: ActivityType.Raid,
    date: new Date('2023-03-10 17:00:00 UTC'),
    releasedWith: Expansions.Lightfall,
    ownership: [Expansions.Lightfall],
    description: `The Conditional Finality exotic shotgun is a random drop from the Root of Nightmares raid. It requires the Lightfall expansion to acquire.`,
    collectible: 2553509474,
    isGuaranteed: false,
    allActivityHashes: [2381413764, 2918919505]
  },
  {
    name: 'Ghosts of the Deep',
    id: ActivityId.GhostsOfTheDeep,
    type: ActivityType.Dungeon,
    date: new Date('2023-05-26 17:00:00 UTC'),
    releasedWith: LightfallSeasonHashes.Deep,
    ownership: [LightfallDungeonHashes.GhostsOfTheDeep],
    description: `The Navigator exotic trace rifle is a random drop from the Ghosts of the Deep dungeon. It requires the Lightfall Dungeon Key to acquire.`,
    collectible: 161963863,
    isGuaranteed: false,
    allActivityHashes: [313828469, 2716998124]
  },
  {
    name: 'Crota\'s End',
    id: ActivityId.CrotasEnd,
    type: ActivityType.Raid,
    date: new Date('2023-09-01 17:00:00 UTC'),
    releasedWith: LightfallSeasonHashes.Witch,
    ownership: [],
    description: `The Necrochasm exotic auto rifle is a random drop from the Crota's End raid. It is available to all players.`,
    collectible: 203521123,
    isGuaranteed: false,
    allActivityHashes: [156253568, 1507509200, 4179289725]
  },
  {
    name: 'Warlord\'s Ruin',
    id: ActivityId.WarlordsRuin,
    type: ActivityType.Dungeon,
    date: new Date('2023-12-01 17:00:00 UTC'),
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [LightfallDungeonHashes.WarlordsRuin],
    description: `The Buried Bloodline exotic sidearm is a random drop from the Warlord's Ruin dungeon. It requires the Lightfall Dungeon Key to acquire.`,
    collectible: 3275654322,
    isGuaranteed: false,
    allActivityHashes: [2004855007, 2534833093]
  },
  {
    name: 'Starcrossed',
    id: ActivityId.Starcrossed,
    type: ActivityType.ExoticMission,
    date: new Date('2023-12-19 17:00:00 UTC'),
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [LightfallSeasonHashes.Wish],
    description: `The Wish-Keeper exotic bow is a guaranteed drop from the Starcrossed exotic mission. It requires the Lightfall expansion or the Season of the Wish to acquire.`,
    collectible: 3826612761,
    isGuaranteed: true,
    allActivityHashes: [896748846, 1013336498]
  },
  {
    name: 'The Whisper',
    id: ActivityId.TheWhisper,
    type: ActivityType.ExoticMission,
    date: new Date('2024-04-09 17:00:00 UTC'),
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [],
    description: `The Whisper of the Worm exotic sniper rifle is a guaranteed drop from The Whisper exotic mission. It is available to all players.`,
    collectible: 1763610692,
    isGuaranteed: true,
    allActivityHashes: [3743446313, 3871520787]
  },
  {
    name: 'Zero Hour',
    id: ActivityId.ZeroHour,
    type: ActivityType.ExoticMission,
    date: new Date('2024-05-14 17:00:00 UTC'),
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [],
    description: `The Outbreak Perfected exotic pulse rifle is a guaranteed drop from the Zero Hour exotic mission. It is available to all players.`,
    collectible: 360254771,
    isGuaranteed: true,
    allActivityHashes: [3361746271, 1848771417]
  },
  {
    name: 'Salvation\'s Edge',
    id: ActivityId.SalvationsEdge,
    type: ActivityType.Raid,
    date: new Date('2024-06-07 17:00:00 UTC'),
    releasedWith: Expansions.FinalShape,
    ownership: [Expansions.FinalShape],
    description: `The Euphony exotic trace rifle is a random drop from the Salvation's Edge raid. It requires The Final Shape expansion to acquire.`,
    collectible: 3411864064,
    isGuaranteed: false,
    allActivityHashes: [2192826039, 1541433876]
  },
  {
    name: 'Encore',
    id: ActivityId.Encore,
    type: ActivityType.ExoticMission,
    date: new Date('2024-08-27 17:00:00 UTC'),
    releasedWith: FinalShapeEpisodeHashes.Echoes,
    ownership: [FinalShapeEpisodeHashes.Echoes],
    description: `The Choir of One exotic auto rifle is a guaranteed drop from the Encore exotic mission. It requires Episode: Echoes to acquire.`,
    collectible: 2176629195,
    isGuaranteed: true,
    allActivityHashes: [1550266704, 3542111804, 3542111805, 3542111807, 655052177, 2741939455, 2362862865, 2362862867, 2362862866]
  },
  {
    name: 'Vesper\'s Host',
    id: ActivityId.VespersHost,
    type: ActivityType.Dungeon,
    date: new Date('2024-10-11 17:00:00 UTC'),
    releasedWith: FinalShapeEpisodeHashes.Revenant,
    ownership: [FinalShapeDungeonHashes.VespersHost],
    description: `The Ice Breaker exotic sniper rifle is a random drop from the Vesper's Host dungeon. It requires the Final Shape Dungeon Key to acquire.`,
    collectible: 1643809765,
    isGuaranteed: false,
    allActivityHashes: [300092127, 4293676253]
  },
  {
    name: 'Kell\'s Fall',
    id: ActivityId.KellsFall,
    type: ActivityType.ExoticMission,
    date: new Date('2025-01-07 17:00:00 UTC'),
    releasedWith: FinalShapeEpisodeHashes.Revenant,
    ownership: [FinalShapeEpisodeHashes.Revenant],
    description: `The Slayer's Fang exotic shotgun is a guaranteed drop from the Kell's Fall exotic mission. It requires Episode: Revenant to acquire.`,
    collectible: 2454134342,
    isGuaranteed: true,
    allActivityHashes: [3212612420, 3296787721, 715393254, 1583447699, 133833536, 264074906, 1044034163, 3447248293]
  },
  {
    name: 'Sundered Doctrine',
    id: ActivityId.SunderedDoctrine,
    type: ActivityType.Dungeon,
    date: new Date('2025-02-04 17:00:00 UTC'),
    releasedWith: FinalShapeEpisodeHashes.Heresy,
    ownership: [FinalShapeDungeonHashes.SunderedDoctrine],
    description: `The Finality's Auger exotic linear fusion rifle is a random drop from the Sundered Doctrine dungeon. It requires the Final Shape Dungeon Key to acquire.`,
    collectible: 1585644854,
    isGuaranteed: false,
    allActivityHashes: [247869137, 3521648250, 3834447244]
  },
  {
    name: 'Derealize',
    id: ActivityId.Derealize,
    type: ActivityType.ExoticMission,
    date: new Date('2025-02-11 17:00:00 UTC'),
    releasedWith: FinalShapeEpisodeHashes.Heresy,
    ownership: [FinalShapeEpisodeHashes.Heresy],
    description: `The Barrow-Dyad exotic submachine gun is a guaranteed drop from the Derealize exotic mission. It requires Episode: Heresy to acquire.`,
    collectible: 2756203682,
    isGuaranteed: true,
    allActivityHashes: [1503312592]
  },
]
export const otherExotics: ExoticWeapon[] = [
  // World
  {
    name: 'Sweet Business',
    description: 'The Sweet Business exotic auto rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 564802917,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.AutoRifle,
    ownership: []
  },
  {
    name: 'Vigilance Wing',
    description: 'The Vigilance Wing exotic pulse rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 564802919,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.PulseRifle,
    ownership: []
  },
  {
    name: 'Crimson',
    description: 'The Crimson exotic hand cannon is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 564802912,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.HandCannon,
    ownership: []
  },
  {
    name: 'The Jade Rabbit',
    description: 'The Jade Rabbit exotic scout rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 564802915,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.ScoutRifle,
    ownership: []
  },
  {
    name: 'The Huckleberry',
    description: 'The Huckleberry exotic submachine gun is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 564802914,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.SubmachineGun,
    ownership: []
  },
  {
    name: 'SUROS Regime',
    description: 'The SUROS Regime exotic auto rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 564802925,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.AutoRifle,
    ownership: []
  },
  {
    name: 'Coldheart',
    description: 'The Coldheart exotic trace rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028070,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.TraceRifle,
    ownership: []
  },
  {
    name: 'Merciless',
    description: 'The Merciless exotic fusion rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028078,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Solar,
    weaponType: ExoticType.FusionRifle,
    ownership: []
  },
  {
    name: 'Fighting Lion',
    description: 'The Fighting Lion exotic grenade launcher is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028071,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Void,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: []
  },
  {
    name: 'Sunshot',
    description: 'The Sunshot exotic hand cannon is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028068,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Solar,
    weaponType: ExoticType.HandCannon,
    ownership: []
  },
  {
    name: 'Graviton Lance',
    description: 'The Graviton Lance exotic pulse rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028069,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Void,
    weaponType: ExoticType.PulseRifle,
    ownership: []
  },
  {
    name: 'Skyburner\'s Oath',
    description: 'The Skyburner\'s Oath exotic scout rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028066,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Solar,
    weaponType: ExoticType.ScoutRifle,
    ownership: []
  },
  {
    name: 'Borealis',
    description: 'The Borealis exotic sniper rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1642951317,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.SniperRifle,
    ownership: []
  },
  {
    name: 'Riskrunner',
    description: 'The Riskrunner exotic submachine gun is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028067,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.SubmachineGun,
    ownership: []
  },
  {
    name: 'Hard Light',
    description: 'The Hard Light exotic auto rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1657028064,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.AutoRifle,
    ownership: []
  },
  {
    name: 'Prometheus Lens',
    description: 'The Prometheus Lens exotic trace rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1642951316,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Solar,
    weaponType: ExoticType.TraceRifle,
    ownership: []
  },
  {
    name: 'Telesto',
    description: 'The Telesto exotic fusion rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1642951319,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Void,
    weaponType: ExoticType.FusionRifle,
    ownership: []
  },
  {
    name: 'The Prospector',
    description: 'The Prospector exotic grenade launcher is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 199171391,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: []
  },
  {
    name: 'The Wardcliff Coil',
    description: 'The Wardcliff Coil exotic rocket launcher is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 199171390,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.RocketLauncher,
    ownership: []
  },
  {
    name: 'Tractor Cannon',
    description: 'The Tractor Cannon exotic shotgun is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 2094776121,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Void,
    weaponType: ExoticType.Shotgun,
    ownership: []
  },
  {
    name: 'D.A.R.C.I.',
    description: 'The D.A.R.C.I. exotic sniper rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 3695595899,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.SniperRifle,
    ownership: []
  },
  {
    name: 'The Colony',
    description: 'The Colony exotic grenade launcher is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 199171388,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Void,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: []
  },
  {
    name: 'Thunderlord',
    description: 'The Thunderlord exotic light machine gun is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 3249389111,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.RedWar_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.MachineGun,
    ownership: []
  },
  {
    name: 'Cerberus+1',
    description: 'The Cerberus+1 exotic auto rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 564802924,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.AutoRifle,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Arbalest',
    description: 'The Arbalest exotic linear fusion rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 2036397919,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.LinearFusionRifle,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Trinity Ghoul',
    description: 'The Trinity Ghoul exotic bow is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1642951312,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.Bow,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Wavesplitter',
    description: 'The Wavesplitter exotic trace rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1642951315,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Void,
    weaponType: ExoticType.TraceRifle,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Lord of Wolves',
    description: 'The Lord of Wolves exotic shotgun is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1642951314,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Solar,
    weaponType: ExoticType.Shotgun,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Two-Tailed Fox',
    description: 'The Two-Tailed Fox exotic rocket launcher is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 199171384,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Solar,
    weaponType: ExoticType.RocketLauncher,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Black Talon',
    description: 'The Black Talon exotic sword is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 199171383,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Void,
    weaponType: ExoticType.Sword,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'The Queenbreaker',
    description: 'The Queenbreaker exotic linear fusion rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 199171382,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Forsaken_World,
    element: ExoticElement.Arc,
    weaponType: ExoticType.LinearFusionRifle,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Monte Carlo',
    description: 'The Monte Carlo exotic auto rifle is a random drop from any generic Exotic Engram or at the end of activities.',
    collectible: 1501322721,
    type: ExoticSourceType.WorldDrop,
    source: ExoticSourceExpansion.Shadowkeep_World,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.AutoRifle,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Lodestar',
    description: 'The Lodestar exotic trace rifle is obtainable in the Season Pass during Episode: Heresy.',
    collectible: 2521113225,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.FinalShape_Season,
    element: ExoticElement.Arc,
    weaponType: ExoticType.TraceRifle,
    ownership: [Expansions.FinalShape]
  },

  // Archive
  {
    name: 'Sturm',
    description: 'The Sturm exotic hand cannon is obtainable in the Monument to Lost Light archives.',
    collectible: 564802916,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.RedWar_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.HandCannon,
    ownership: []
  },
  {
    name: 'Rat King',
    description: 'The Rat King exotic sidearm is obtainable in the Monument to Lost Light archives.',
    collectible: 564802918,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.RedWar_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.Sidearm,
    ownership: []
  },
  {
    name: 'MIDA Multi-Tool',
    description: 'The MIDA Multi-Tool exotic scout rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 564802913,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.RedWar_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.ScoutRifle,
    ownership: []
  },
  {
    name: 'Polaris Lance',
    description: 'The Polaris Lance exotic scout rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 1642951318,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.RedWar_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.ScoutRifle,
    ownership: []
  },
  {
    name: 'Legend of Acrius',
    description: 'The Legend of Acrius exotic shotgun is obtainable in the Monument to Lost Light archives.',
    collectible: 199171389,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.RedWar_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.Shotgun,
    ownership: []
  },
  {
    name: 'Worldline Zero',
    description: 'The Worldline Zero exotic sword is obtainable in the Monument to Lost Light archives.',
    collectible: 199171387,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.RedWar_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.Sword,
    ownership: []
  },
  {
    name: 'Sleeper Simulant',
    description: 'The Sleeper Simulant exotic linear fusion rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 199171386,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.RedWar_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.LinearFusionRifle,
    ownership: []
  },
  {
    name: 'Ace of Space',
    description: 'The Ace of Space exotic hand cannon is obtainable in the Monument to Lost Light archives.',
    collectible: 1660030046,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.HandCannon,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Izanagi\'s Burden',
    description: 'The Izanagi\'s Burden exotic sniper rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 24541428,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.SniperRifle,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'The Last Word',
    description: 'The Last Word exotic hand cannon is obtainable in the Monument to Lost Light archives.',
    collectible: 3074058273,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.HandCannon,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Thorn',
    description: 'The Thorn exotic hand cannon is obtainable in the Monument to Lost Light archives.',
    collectible: 4009683574,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.HandCannon,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Lumina',
    description: 'The Lumina exotic hand cannon is obtainable in the Monument to Lost Light archives.',
    collectible: 2924632392,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.HandCannon,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Bad Juju',
    description: 'The Bad Juju exotic pulse rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 4207100358,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.PulseRifle,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Le Monarque',
    description: 'The Le Monarque exotic bow is obtainable in the Monument to Lost Light archives.',
    collectible: 3573051804,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Void,
    weaponType: ExoticType.Bow,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Jötunn',
    description: 'The Jötunn exotic fusion rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 3584311877,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.FusionRifle,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Tarrabah',
    description: 'The Tarrabah exotic submachine gun is obtainable in the Monument to Lost Light archives.',
    collectible: 2329697053,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.SubmachineGun,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Anarchy',
    description: 'The Anarchy exotic grenade launcher is obtainable in the Monument to Lost Light archives.',
    collectible: 2220014607,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Truth',
    description: 'The Truth exotic rocket launcher is obtainable in the Monument to Lost Light archives.',
    collectible: 1763840761,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Archive,
    element: ExoticElement.Void,
    weaponType: ExoticType.RocketLauncher,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Bastion',
    description: 'The Bastion exotic fusion rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 3207791447,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.FusionRifle,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Witherhoard',
    description: 'The Witherhoard exotic grenade launcher is obtainable in the Monument to Lost Light archives.',
    collectible: 1250332035,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Traveler\'s Chosen',
    description: 'The Traveler\'s Chosen exotic sidearm is obtainable in the Monument to Lost Light archives.',
    collectible: 1370087379,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.Sidearm,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Eriana\'s Vow',
    description: 'The Eriana\'s Vow exotic hand cannon is obtainable in the Monument to Lost Light archives.',
    collectible: 2741465947,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.HandCannon,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Symmetry',
    description: 'The Symmetry exotic scout rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 47014674,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.ScoutRifle,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Devil\'s Ruin',
    description: 'The Devil\'s Ruin exotic sidearm is obtainable in the Monument to Lost Light archives.',
    collectible: 2190071629,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.Sidearm,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Tommy\'s Matchbook',
    description: 'The Tommy\'s Matchbook exotic auto rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 778561967,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.AutoRifle,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'The Fourth Horseman',
    description: 'The Fourth Horseman exotic shotgun is obtainable in the Monument to Lost Light archives.',
    collectible: 2318862156,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.Shotgun,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Ruinous Effigy',
    description: 'The Ruinous Effigy exotic trace rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 1392294260,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Void,
    weaponType: ExoticType.TraceRifle,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Leviathan\'s Breath',
    description: 'The Leviathan\'s Breath exotic bow is obtainable in the Monument to Lost Light archives.',
    collectible: 3552855013,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Shadowkeep_Archive,
    element: ExoticElement.Void,
    weaponType: ExoticType.Bow,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'Cryosthesia 77k',
    description: 'The Cryosthesia 77k exotic sidearm is obtainable in the Monument to Lost Light archives.',
    collectible: 2817803243,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.BeyondLight_Archive,
    element: ExoticElement.Stasis,
    weaponType: ExoticType.Sidearm,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Ager\'s Scepter',
    description: 'The Ager\'s Scepter exotic trace rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 1969234607,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.BeyondLight_Archive,
    element: ExoticElement.Stasis,
    weaponType: ExoticType.TraceRifle,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Duality',
    description: 'The Duality exotic shotgun is obtainable in the Monument to Lost Light archives.',
    collectible: 478622552,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.BeyondLight_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.Shotgun,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Ticuu\'s Divination',
    description: 'The Ticuu\'s Divination exotic bow is obtainable in the Monument to Lost Light archives.',
    collectible: 882572881,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.BeyondLight_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.Bow,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Lorentz Driver',
    description: 'The Lorentz Driver exotic linear fusion rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 3602500492,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.BeyondLight_Archive,
    element: ExoticElement.Void,
    weaponType: ExoticType.LinearFusionRifle,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Grand Overture',
    description: 'The Grand Overture exotic light machine gun is obtainable in the Monument to Lost Light archives.',
    collectible: 1256729926,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.WitchQueen_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.MachineGun,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Trespasser',
    description: 'The Trespasser exotic sidearm is obtainable in the Monument to Lost Light archives.',
    collectible: 77226437,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.WitchQueen_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.Sidearm,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Delicate Tomb',
    description: 'The Delicate Tomb exotic fusion rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 1977134032,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.WitchQueen_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.FusionRifle,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'The Manticore',
    description: 'The Manticore exotic submachine gun is obtainable in the Monument to Lost Light archives.',
    collectible: 985132347,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.WitchQueen_Archive,
    element: ExoticElement.Void,
    weaponType: ExoticType.SubmachineGun,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Verglas Curve',
    description: 'The Verglas Curve exotic bow is obtainable in the Monument to Lost Light archives.',
    collectible: 1184569884,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Lightfall_Archive,
    element: ExoticElement.Stasis,
    weaponType: ExoticType.Bow,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Wicked Implement',
    description: 'The Wicked Implement exotic scout rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 1832123372,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Lightfall_Archive,
    element: ExoticElement.Stasis,
    weaponType: ExoticType.ScoutRifle,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Centrifuse',
    description: 'The Centrifuse exotic auto rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 2746608191,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Lightfall_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.AutoRifle,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Quicksilver Storm',
    description: 'The Quicksilver Storm exotic auto rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 3709294547,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Lightfall_Archive,
    element: ExoticElement.Strand,
    weaponType: ExoticType.AutoRifle,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Ex Diris',
    description: 'The Ex Diris exotic grenade launcher is obtainable in the Monument to Lost Light archives.',
    collectible: 2375343865,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Lightfall_Archive,
    element: ExoticElement.Arc,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Dragon\'s Breath',
    description: 'The Dragon\'s Breath exotic rocket launcher is obtainable in the Monument to Lost Light archives.',
    collectible: 3019220289,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Lightfall_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.RocketLauncher,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Tessellation',
    description: 'The Tessellation exotic fusion rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 461401987,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.FinalShape_Archive,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.FusionRifle,
    ownership: [Expansions.FinalShape]
  },
  {
    name: 'Red Death Reformed',
    description: 'The Red Death Reformed exotic pulse rifle is obtainable in the Monument to Lost Light archives.',
    collectible: 173880226,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.FinalShape_Archive,
    element: ExoticElement.Solar,
    weaponType: ExoticType.PulseRifle,
    ownership: [Expansions.FinalShape]
  },
  {
    name: 'Alethonym',
    description: 'The Alethonym exotic grenade launcher is obtainable in the Monument to Lost Light archives.',
    collectible: 2081206839,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.FinalShape_Archive,
    element: ExoticElement.Stasis,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: [Expansions.FinalShape]
  },

  // Quest
  {
    name: 'Malfeasance',
    description: 'The Malfeasance exotic hand cannon is obtainable in a quest with The Drifter.',
    collectible: 1660030045,
    type: ExoticSourceType.Archive,
    source: ExoticSourceExpansion.Forsaken_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.HandCannon,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'The Chaperone',
    description: 'The Chaperone exotic shotgun is obtainable in a quest with Shaw Han.',
    collectible: 1660030047,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.Forsaken_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.Shotgun,
    ownership: [Expansions.Forsaken]
  },
  {
    name: 'Deathbringer',
    description: 'The Deathbringer exotic rocket launcher is obtainable in a quest with Eris Morn.',
    collectible: 888224289,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.Shadowkeep_Quest,
    element: ExoticElement.Void,
    weaponType: ExoticType.RocketLauncher,
    ownership: [Expansions.Shadowkeep]
  },
  {
    name: 'No Time to Explain',
    description: 'The No Time to Explain exotic pulse rifle is obtainable by completing the Beyond Light campaign.',
    collectible: 2107308931,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.BeyondLight_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.PulseRifle,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Forerunner',
    description: 'The Forerunner exotic sidearm is obtainable in a quest with Xûr.',
    collectible: 1028725073,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.BeyondLight_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.Sidearm,
    ownership: [Expansions.Anniversary30th]
  },
  {
    name: 'Cloudstrike',
    description: 'The Cloudstrike exotic sniper rifle is obtainable as a random drop in Empire Hunts.',
    collectible: 396432035,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.BeyondLight_Quest,
    element: ExoticElement.Arc,
    weaponType: ExoticType.SniperRifle,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Salvation\'s Grip',
    description: 'The Salvation\'s Grip exotic grenade launcher is obtainable in a quest with Variks.',
    collectible: 2289501267,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.BeyondLight_Quest,
    element: ExoticElement.Stasis,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'The Lament',
    description: 'The Lament exotic sword is obtainable in a quest with Variks.',
    collectible: 3935854305,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.BeyondLight_Quest,
    element: ExoticElement.Solar,
    weaponType: ExoticType.Sword,
    ownership: [Expansions.BeyondLight]
  },
  {
    name: 'Osteo Striga',
    description: 'The Osteo Striga exotic submachine gun is obtainable by completing The Witch Queen campaign.',
    collectible: 1100865290,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.WitchQueen_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.SubmachineGun,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Edge of Concurrence',
    description: 'The Edge of Concurrence exotic glaive is obtainable in a quest with Fynch.',
    collectible: 328283190,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.WitchQueen_Quest,
    element: ExoticElement.Arc,
    weaponType: ExoticType.Glaive,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Edge of Action',
    description: 'The Edge of Action exotic glaive is obtainable in a quest with Fynch.',
    collectible: 3810283242,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.WitchQueen_Quest,
    element: ExoticElement.Void,
    weaponType: ExoticType.Glaive,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Edge of Intent',
    description: 'The Edge of Intent exotic glaive is obtainable in a quest with Fynch.',
    collectible: 1089205875,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.WitchQueen_Quest,
    element: ExoticElement.Solar,
    weaponType: ExoticType.Glaive,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Parasite',
    description: 'The Parasite exotic grenade launcher is obtainable in a quest with Fynch.',
    collectible: 4028619089,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.WitchQueen_Quest,
    element: ExoticElement.Solar,
    weaponType: ExoticType.GrenadeLauncher,
    ownership: [Expansions.TheWitchQueen]
  },
  {
    name: 'Final Warning',
    description: 'The Final Warning exotic sidearm is obtainable in a quest with Nimbus.',
    collectible: 2843753795,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.Lightfall_Quest,
    element: ExoticElement.Strand,
    weaponType: ExoticType.Sidearm,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Deterministic Chaos',
    description: 'The Deterministic Chaos exotic light machine gun is obtainable in a quest with Nimbus.',
    collectible: 4226434173,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.Lightfall_Quest,
    element: ExoticElement.Void,
    weaponType: ExoticType.MachineGun,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Winterbite',
    description: 'The Winterbite exotic glaive is obtainable in a quest with Nimbus.',
    collectible: 2629609053,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.Lightfall_Quest,
    element: ExoticElement.Stasis,
    weaponType: ExoticType.Glaive,
    ownership: [Expansions.Lightfall]
  },
  {
    name: 'Khvostov 7G-0X',
    description: 'The Khvostov 7G-0X exotic auto rifle is obtainable in a quest with Ghost.',
    collectible: 1578345208,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.FinalShape_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.AutoRifle,
    ownership: [Expansions.FinalShape]
  },
  {
    name: 'Still Hunt',
    description: 'The Still Hunt exotic sniper rifle is obtainable in a quest with Ghost.',
    collectible: 2289185883,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.FinalShape_Quest,
    element: ExoticElement.Solar,
    weaponType: ExoticType.SniperRifle,
    ownership: [Expansions.FinalShape]
  },
  {
    name: 'Ergo Sum',
    description: 'The Ergo Sum exotic sword is obtainable in a quest with Ghost.',
    collectible: 740275358,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.FinalShape_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.Sword,
    ownership: [Expansions.FinalShape]
  },
  {
    name: 'Microcosm',
    description: 'The Microcosm exotic trace rifle is obtainable in a quest with Ghost.',
    collectible: 3613141427,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.FinalShape_Quest,
    element: ExoticElement.Kinetic,
    weaponType: ExoticType.TraceRifle,
    ownership: [Expansions.FinalShape]
  },
  {
    name: 'Heir Apparent',
    description: 'The Heir Apparent exotic light machine gun is obtainable in a quest during Guardian Games.',
    collectible: 2842076592,
    type: ExoticSourceType.Quest,
    source: ExoticSourceExpansion.Event_Quest,
    element: ExoticElement.Solar,
    weaponType: ExoticType.MachineGun,
    ownership: [Expansions.FinalShape]
  },
]
