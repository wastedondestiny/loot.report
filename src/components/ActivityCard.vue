<script setup lang="ts">
import { computed } from 'vue'
import { Collapsible, TheTooltip, formatNumber } from '@wastedondestiny/destiny-library'
import { ExoticActivity, ActivityType } from '../enums/activities'
import collectibles from '../generated/collectibleDefinitions.json'
import MemberIndicator from './MemberIndicator.vue'
import { Activity, CollectionAccount } from '../types'
import ItemIcon from './ItemIcon.vue'

const emit = defineEmits(['toggle'])
const props = withDefaults(defineProps<{
  activity: ExoticActivity,
  activities: Record<string, Activity[]>,
  reset: Date,
  loading: string[],
  members: CollectionAccount[],
  rotations: Record<number, string>,
  isOpen: { missing: boolean, acquired: boolean, unavailable: boolean },
  isDemo?: boolean
}>(), {
  isDemo: false
})
const now = Date.now()

function isActive(date: Date) {
  return (now - date.getTime()) < 2_629_743_000 // Milliseconds in a month
}

function checkOwnership(member: CollectionAccount) {
  return !props.activity.ownership?.length ||
    props.activity.ownership.some(y => {
      const hasExpansion = ((member.versionsOwned ?? 0) & y) === y
      const hasSeason = member.seasonsOwned?.includes(y)
      const hasDungeon = member.dungeonsOwned?.includes(y)
      return hasExpansion || hasSeason || hasDungeon
    })
}

const activeMembers = computed(() => props.members.filter(x => checkOwnership(x) && isActive(x.dateLastPlayed!)))
const inactiveMembers = computed(() => props.members.filter(x => !checkOwnership(x) || !isActive(x.dateLastPlayed!)))

const activeMembersWhoDontHave = computed(() => activeMembers.value.filter(x => !x.items[props.activity.collectible]))
const activeMembersWhoHave = computed(() => activeMembers.value.filter(x => x.items[props.activity.collectible]))

const inactiveMembersWhoDontHave = computed(() => inactiveMembers.value.filter(x => checkOwnership(x) && !x.items[props.activity.collectible]))
const inactiveMembersWhoHave = computed(() => inactiveMembers.value.filter(x => x.items[props.activity.collectible]))

const membersWhoCantHave = computed(() => inactiveMembers.value.filter(x => !checkOwnership(x) && !x.items[props.activity.collectible]))

const ownershipPercentage = computed(() => props.members.filter(x => x.items[props.activity.collectible]).length / props.members.length)

const completions = computed(() => Object.fromEntries(Object.entries(props.activities).map(([key, val]) => ([key, val
  .filter(x => props.activity.allActivityHashes.includes(x.activityId) && x.completed)
  .reduce((a, y) => { a[y.characterId] = (a[y.characterId] || 0) + 1; return a }, {} as Record<string, number>)
]))))

const collectible = computed(() => collectibles.find(x => x.id === props.activity.collectible))

const isInRotation = computed(() => {
  const rotator = props.rotations[props.activity.id]
  return rotator && rotator.startsWith('Active')
})

function toggle(tag: 'missing'|'acquired'|'unavailable') {
  emit('toggle', tag)
}

function inTime(date: Date) {
  const seconds = Math.floor((date.getTime() - now) / 1_000)

  if (seconds > 500_000_000) return 'in a long time'

  let interval = seconds / 31_536_000
  if (interval > 1) return `${Math.floor(interval)} year${Math.floor(interval) > 1 ? 's' : ''}`

  interval = seconds / 2_592_000
  if (interval > 1) return `${Math.floor(interval)} month${Math.floor(interval) > 1 ? 's' : ''}`

  interval = seconds / 86_400
  if (interval > 1) return `${Math.floor(interval)} day${Math.floor(interval) > 1 ? 's' : ''}`

  interval = seconds / 3_600
  if (interval > 1) return `${Math.floor(interval)} hour${Math.floor(interval) > 1 ? 's' : ''}`

  interval = seconds / 60
  if (interval > 1) return `${Math.floor(interval)} minute${Math.floor(interval) > 1 ? 's' : ''}`

  return `${Math.floor(seconds)} second${Math.floor(seconds) > 1 ? 's' : ''}`
}
</script>

<template>
  <div class="flex flex-col w-72 bg-white text-black shadow-lg shadow-black/50 rounded-2xl px-2 py-3 mb-5 relative">
    <div class="flex items-center justify-between -mt-3 mb-1 w-72 -mx-2 rounded-t-2xl py-1 px-2 bg-zinc-200">
      <div class="flex gap-1">
        <span class="text-3xl leading-3 inline-block w-4 text-center" v-if="isInRotation || activity.isGuaranteed">
          <TheTooltip v-if="activity.isGuaranteed" :no-interact="isDemo">
            <img class="w-4 h-4" src="../assets/free.svg" alt="Guaranteed" />
            <template #icon>
              <img class="w-8 h-8" src="../assets/free.svg" />
            </template>
            <template #content>
              This exotic weapon is guaranteed to drop after completion of this activity or its related quest.
            </template>
          </TheTooltip>
          <TheTooltip v-else-if="isInRotation" :no-interact="isDemo">
            <img class="w-4 h-4" src="../assets/refresh.svg" alt="Farmable" />
            <template #icon>
              <img class="w-8 h-8" src="../assets/refresh.svg" />
            </template>
            <template #content>
              This exotic weapon is farmable this week, and every completion of the activity will have a chance to drop the weapon.
            </template>
          </TheTooltip>
        </span>
        <TheTooltip v-if="activity.type === ActivityType.Raid" :no-interact="isDemo">
          <img class="w-4 h-4" src="../assets/raid.svg" alt="Raid" />
          <template #icon>
            <img class="w-8 h-8" src="../assets/raid.svg" />
          </template>
          <template #content>
            This activity is a raid.
          </template>
        </TheTooltip>
        <TheTooltip v-if="activity.type === ActivityType.Dungeon" :no-interact="isDemo">
          <img class="w-4 h-4" src="../assets/dungeon.png" alt="Dungeon" />
          <template #icon>
            <img class="w-8 h-8" src="../assets/dungeon.png" />
          </template>
          <template #content>
            This activity is a dungeon.
          </template>
        </TheTooltip>
        <TheTooltip v-if="activity.type === ActivityType.ExoticMission" :no-interact="isDemo">
          <img class="w-4 h-4" src="../assets/engram.svg" alt="Exotic mission" />
          <template #icon>
            <img class="w-8 h-8" src="../assets/engram.svg" />
          </template>
          <template #content>
            This activity is an exotic mission.
          </template>
        </TheTooltip>
        <span class="uppercase text-xs">{{ activity.name }}</span>
      </div>
      <span class="h-4 leading-3" v-if="members.length">
        <span class="text-xs"><small class="opacity-75">HAS:</small>{{ formatNumber(Math.floor(ownershipPercentage * 100)) }}%</span>
      </span>
    </div>
    <div class="pb-1 mb-1 border-b-2 border-black/25">
      <TheTooltip :no-interact="isDemo">
        <ItemIcon :item="collectible" large />
        <span class="ml-[3.75rem] mb-1 inline-flex flex-col">
          <h2 class="text-xl font-semibold">{{ collectible?.name }}</h2>
          <small class="text-sm">{{ collectible?.type }}</small>
        </span>
        <template #icon>
          <ItemIcon :item="collectible" />
        </template>
        <template #content>
          <p>{{ activity.description }}</p>
        </template>
      </TheTooltip>
    </div>
    <p class="leading-4 text-xs h-3">Reset in {{ inTime(reset) }}</p>
    <div class="flex flex-col py-1" v-if="members.length">
      <Collapsible name="MISSING THE EXOTIC" :open="isOpen['missing']" @toggle="() => toggle('missing')" :no-interact="isDemo">
        <MemberIndicator v-for="member in activeMembersWhoDontHave" :key="member.membershipId" :repeatable="!!isInRotation" :activity="activity" :account="member" :loading="!loading.includes(member.membershipId)" :activities="completions[member.membershipId]">
          {{ member.displayName }}
        </MemberIndicator>
      </Collapsible>
      <Collapsible name="HAVE THE EXOTIC" :open="isOpen['acquired']" @toggle="() => toggle('acquired')" :no-interact="isDemo">
        <MemberIndicator v-for="member in activeMembersWhoHave" :key="member.membershipId" :repeatable="!!isInRotation" :activity="activity" :account="member" :loading="!loading.includes(member.membershipId)" :activities="completions[member.membershipId]" has-exotic>
          {{ member.displayName }}
        </MemberIndicator>
      </Collapsible>
      <Collapsible name="INACTIVE MEMBERS" :open="isOpen['unavailable']" @toggle="() => toggle('unavailable')" :no-interact="isDemo">
        <MemberIndicator v-for="member in inactiveMembersWhoDontHave" :key="member.membershipId" :activity="activity" :account="member">
          {{ member.displayName }}
        </MemberIndicator>
        <MemberIndicator v-for="member in membersWhoCantHave" :key="member.membershipId" :activity="activity" :account="member" has-no-access>
          {{ member.displayName }}
        </MemberIndicator>
        <MemberIndicator v-for="member in inactiveMembersWhoHave" :key="member.membershipId" :activity="activity" :account="member" has-exotic :has-no-access="!checkOwnership(member)">
          {{ member.displayName }}
        </MemberIndicator>
      </Collapsible>
    </div>
    <template v-if="isDemo">
      <TheTooltip class="absolute" style="top: 0px; right: 2px;">
        <div class="border-blue-500 border-2 rounded-md" style="height: 24px; width: 58px;" />
        <template #content>
          <p class="w-52">This number represents the percentage of clan members who have acquired this weapon.</p>
        </template>
      </TheTooltip>
      <TheTooltip class="absolute" style="top: 86px; left: 2px;">
        <div class="border-blue-500 border-2 rounded-md" style="height: 24px; width: 96px;" />
        <template #content>
          <p class="w-52">The reset happens every tuesday at 1700 UTC. After that happens, you will be able to try again for exotic weapons you do not have, and the current farmable activities will change. The available exotic mission will change as well.</p>
        </template>
      </TheTooltip>
      <TheTooltip class="absolute" style="top: 136px; right: 10px;">
        <div class="border-blue-500 border-2 rounded-md" style="height: 124px; width: 46px;" />
        <template #content>
          <p class="w-52">This is an indicator for how many characters you have, and how many of them have tried to obtain the exotic weapon. When the dot is black, the activity has been tried on a character. When the exotic is a guaranteed drop, there is only ever one dot. When the activity is farmable, this will instead become a numeric counter.</p>
        </template>
      </TheTooltip>
      <TheTooltip class="absolute" style="top: 294px; left: 2px;">
        <div class="border-blue-500 border-2 rounded-md" style="height: 24px; width: 284px;" />
        <template #content>
          <p class="w-52">This category includes all players who do not have access to the activity from which to acquire this exotic, or may or may not have acquired it but have not played for at least a month.</p>
        </template>
      </TheTooltip>
    </template>
  </div>
</template>