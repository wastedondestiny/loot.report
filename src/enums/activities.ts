import { ReleaseId } from './releases'

export enum ActivityId {
  TheWhisper = 3743446313,
  LastWish = 2122313384,
  ShatteredThrone = 2032534090,
  ZeroHour = 3361746271,
  GardenOfSalvation = 1042180643,
  PitOfHeresy = 2582501063,
  DeepStoneCrypt = 910380154,
  Presage = 3883295757,
  VaultOfGlass = 3881495763,
  GraspOfAvarice = 4078656646,
  VoxObscura = 2668737148,
  VowOfTheDisciple = 1441982566,
  Duality = 2823159265,
  KingsFall = 1374392663,
  SpireOfTheWatcher = 1262462921,
  OperationSeraphShield = 1221538367,
  NodeOvrdAvalon = 3755529435,
  RootOfNightmares = 2381413764,
  GhostsOfTheDeep = 313828469,
  CrotasEnd = 4179289725,
  WarlordsRuin = 2004855007,
  Starcrossed = 896748846,
  SalvationsEdge = 1541433876,
  Encore = 1550266704,
  KellsFall = 3212612420,
  VespersHost = 300092127,
  SunderedDoctrine = 3834447244,
  Derealize,
}

export enum ActivityType {
  Raid,
  Dungeon,
  ExoticMission
}

export type ExoticActivity = {
  name: string,
  id: ActivityId,
  type: ActivityType,
  date: Date,
  releasedWith: ReleaseId,
  ownership: ReleaseId[],
  description: string,
  collectible: number,
  isGuaranteed: boolean,
  allActivityHashes: number[]
}
