export enum Expansions {
  Destiny2 = 1,
  CurseOfOsiris = 2,
  Warmind = 4,
  Forsaken = 8,
  AnnualPass = 16,
  Shadowkeep = 32,
  BeyondLight = 64,
  Anniversary30th = 128,
  TheWitchQueen = 256,
  Lightfall = 512,
  FinalShape = 1024
}
