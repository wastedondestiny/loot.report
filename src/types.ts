import { type Account } from '@wastedondestiny/destiny-library'

export type CollectionAccount = Account & {
  items: Record<string, boolean>
}

export type Activity = {
  id: string,
  modes: number[],
  completed: boolean,
  activityId: number,
  characterId: string
}

export type LeaderboardPage = {
  players: CollectionAccount[],
  page: number,
  count: number,
  total: number
}
