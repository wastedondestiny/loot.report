import { Expansions } from './expansions'
import { BeyondLightSeasonHashes, FinalShapeEpisodeHashes, LightfallSeasonHashes, ShadowkeepSeasonHashes, WitchQueenSeasonHashes } from './seasons'
import { FinalShapeDungeonHashes, LightfallDungeonHashes, WitchQueenDungeonHashes } from './dungeons'

export type SeasonId = ShadowkeepSeasonHashes | BeyondLightSeasonHashes | WitchQueenSeasonHashes | LightfallSeasonHashes
export type DungeonId = WitchQueenDungeonHashes | LightfallDungeonHashes | FinalShapeDungeonHashes
export type ReleaseId = Expansions | SeasonId | DungeonId | FinalShapeEpisodeHashes

export enum ExoticSourceType {
  WorldDrop, // Sweet Business, Vigilance Wing, Crimson, The Jade Rabbit, The Huckleberry, SUROS Regime, Coldheart, Merciless, Fighting Lion, Sunshot, Graviton Lance, Skyburner's Oath, Borealis, Riskrunner, Hard Light, Prometheus Lens, Telesto, The Prospector, The Wardcliff Coil, Tractor Cannon, D.A.R.C.I., The Colony, Cerberus+1, Arbalest, Trinity Ghoul, Wavesplitter, Lord of Wolves, Two-Tailed Fox, Black Talon, The Queenbreaker, Monte Carlo
  Archive, // Sturm, Rat King, MIDA Multi-Tool, Polaris Lance, Legend of Acrius, Worldline Zero, Sleeper Simulant, Ace of Space, Izanagi's Burden, The Last Word, Thorn, Lumina, Bad Juju, Le Monarque, Jötunn, Tarrabah, Anarchy, Truth, Bastion, Witherhoard, Traveler's Chosen, Eriana's Vow, Symmetry, Devil's Ruin, Tommy's Matchbook, The Fourth Horseman, Ruinous Effigy, Leviathan's Breath, Grand Overture, Cryosthesia 77k, Ager's Scepter, Duality, Ticuu's Divination, Lorentz Driver, Trespasser, Delicate Tomb, The Manticore, Verglas Curve, Wicked Implement, Centrifuse, Quicksilver Storm, Ex Diris, Dragon's Breath, Tesselation, Red Death Reformed
  Quest, // Malfeasance, The Chaperone, Deathbringer, No Time to Explain, Forerunner, Cloudstrike, Salvation's Grip, The Lament, Osteo Striga, Edge of Concurrence, Edge of Action, Edge of Intent, Parasite, Final Warning, Deterministic Chaos, Winterbite, Khvostov 7G-0X, Still Hunt, Ergo Sum, Microcosm, Thunderlord, Heir Apparent, Alethonym
}
export enum ExoticSourceExpansion {
  // WORLD DROPS
  RedWar_World, // Sweet Business, Vigilance Wing, Crimson, The Jade Rabbit, The Huckleberry, SUROS Regime, Coldheart, Merciless, Fighting Lion, Sunshot, Graviton Lance, Skyburner's Oath, Borealis, Riskrunner, Hard Light, Prometheus Lens, Telesto, The Prospector, The Wardcliff Coil, Tractor Cannon, D.A.R.C.I., The Colony
  Forsaken_World, // Cerberus+1, Arbalest, Trinity Ghoul, Wavesplitter, Lord of Wolves, Two-Tailed Fox, Black Talon, The Queenbreaker
  Shadowkeep_World, // Monte Carlo

  // ARCHIVE
  RedWar_Archive, // Sturm, Rat King, MIDA Multi-Tool, Polaris Lance, Legend of Acrius, Worldline Zero, Sleeper Simulant
  Forsaken_Archive, // Ace of Space, Izanagi's Burden, The Last Word, Thorn, Lumina, Bad Juju, Le Monarque, Jötunn, Tarrabah, Anarchy, Truth
  Shadowkeep_Archive, // Bastion, Witherhoard, Traveler's Chosen, Eriana's Vow, Symmetry, Devil's Ruin, Tommy's Matchbook, The Fourth Horseman, Ruinous Effigy, Leviathan's Breath
  BeyondLight_Archive, // Grand Overture, Cryosthesia 77k, Ager's Scepter, Duality, Ticuu's Divination, Lorentz Driver
  WitchQueen_Archive, // Trespasser, Delicate Tomb, The Manticore
  Lightfall_Archive, // Verglas Curve, Wicked Implement, Centrifuse, Quicksilver Storm, Ex Diris, Dragon's Breath
  FinalShape_Archive, // Tesselation, Red Death Reformed

  // QUESTS
  Forsaken_Quest, // Malfeasance, The Chaperone
  Shadowkeep_Quest, // Deathbringer
  BeyondLight_Quest, // No Time to Explain, Forerunner, Cloudstrike, Salvation's Grip, The Lament
  WitchQueen_Quest, // Osteo Striga, Edge of Concurrence, Edge of Action, Edge of Intent, Parasite
  Lightfall_Quest, // Final Warning, Deterministic Chaos, Winterbite
  FinalShape_Quest, // Khvostov 7G-0X, Still Hunt, Ergo Sum, Microcosm
  Event_Quest, // Thunderlord, Heir Apparent

  // CURRENT SEASON
  FinalShape_Season, // Alethonym
}
export enum ExoticElement {
  Kinetic,
  Arc,
  Solar,
  Void,
  Stasis,
  Strand
}
export enum ExoticType {
  AutoRifle,
  ScoutRifle,
  PulseRifle,
  TraceRifle,
  HandCannon,
  Bow,
  SubmachineGun,
  Sidearm,
  GrenadeLauncher,
  FusionRifle,
  SniperRifle,
  Shotgun,
  LinearFusionRifle,
  RocketLauncher,
  Sword,
  MachineGun,
  Glaive
}
export type ExoticWeapon = {
  name: string,
  description: string,
  collectible: number,
  type: ExoticSourceType,
  source: ExoticSourceExpansion,
  element: ExoticElement,
  weaponType: ExoticType
  ownership: ReleaseId[]
}
