/// <reference types="vite/client" />

interface Window {
  adsbygoogle: {[key: string]: unknown}[],
  googleAdsClient: string,
  googleAdsSlot: string,
  dataLayer: {[key: string]: unknown}[]
}
