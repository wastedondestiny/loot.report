import express from 'express'
import fs from 'node:fs/promises'
import path from 'node:path'
import url from 'node:url'
import puppeteer from 'puppeteer'
import resizeImg from 'resize-image-buffer'
import dotenv from 'dotenv'
dotenv.config()

const __filename = url.fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const intercept = request => {
  if (request.isInterceptResolutionHandled()) return
  const url = request.url()
  const filters = ['/news', 'google', 'doubleclick']
  const shouldAbort = filters.some(urlPart => url.includes(urlPart))
  if (shouldAbort) request.abort()
  else request.continue()
}

const prerender = async () => {
  // Create a simple Express server that prints "Hello, World"
  const app = express()
  app.use(express.static('./dist'))
  app.use((_, res) => { res.status(404).sendFile(path.join(__dirname, '../dist/404.html')) })
  const server = app.listen() // Take a random available port
  
  // Launch Puppeteer and navigate to the Express server
  const browser = await puppeteer.launch({ slowMo: 500, headless: 'new' })

  const indexPage = await browser.newPage()
  await indexPage.setRequestInterception(true)
  indexPage.on('request', intercept)
  await indexPage.goto(`http://localhost:${server.address().port}`)
  const indexContent = await indexPage.content()
  await fs.writeFile('./dist/index.html', indexContent)

  // Generate avatar-like image (resize to 1080x1080 and 270x270)
  indexPage.evaluate(() => {
    const logo = document.querySelector('#Layer_1')
    document.querySelector('#app').replaceWith(logo)
    logo.classList = 'w-full h-auto mx-auto'
    logo.style.paddingTop = '22.5%'
    logo.style.width = '55%'
  })
  indexPage.setViewport({ width: 1920, height: 1080 })
  const ogBuffer = Buffer.from((await indexPage.screenshot()).buffer)
  await fs.writeFile('./public/og.png', ogBuffer)
  await fs.writeFile('./dist/og.png', ogBuffer)

  // Generate open graph image (resize to 1920x1080)
  indexPage.evaluate(() => {
    const logo = document.querySelector('#Layer_1')
    logo.classList = 'w-auto h-full mx-auto'
    logo.style.paddingTop = '12.5%'
    logo.style.width = null
    logo.style.height = '87.5%'
    logo.setAttribute('viewBox', '0 0 147.5 169')
  })
  indexPage.setViewport({ width: 1080, height: 1080 })
  const bigAvatarBuffer = Buffer.from((await indexPage.screenshot()).buffer)
  await fs.writeFile('./public/big_avatar.png', bigAvatarBuffer)
  await fs.writeFile('./dist/big_avatar.png', bigAvatarBuffer)
  const avatarBuffer = await resizeImg(bigAvatarBuffer, { width: 270, height: 270 })
  await fs.writeFile('./public/avatar.png', avatarBuffer)
  await fs.writeFile('./dist/avatar.png', avatarBuffer)

  // Cleanup 
  await browser.close()
  server.close()
}

prerender()
