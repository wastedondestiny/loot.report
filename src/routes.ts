import { createRouter, createWebHistory } from 'vue-router'
import Home from './components/Home.vue'
// import AboutPage from './components/AboutPage.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      name: 'Home',
      path: '/:clan?',
      component: Home
    },
    /* {
      name: 'AboutPage',
      path: '/about',
      component: AboutPage
    }, */
  ]
})

export default router
