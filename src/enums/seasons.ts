export enum ShadowkeepSeasonHashes {
  Undying = 1743682818,
  Dawn = 1743682819,
  Worthy = 2809059425,
  Arrivals = 2809059424
}

export enum BeyondLightSeasonHashes {
  Hunt = 2809059427,
  Chosen = 2809059426,
  Splicer = 2809059429,
  Lost = 2809059428
}

export enum WitchQueenSeasonHashes {
  Risen = 2809059431,
  Haunted = 2809059430,
  Plunder = 2809059433,
  Seraph = 2809059432
}

export enum LightfallSeasonHashes {
  Defiance = 2758726568,
  Deep = 2758726569,
  Witch = 2758726570,
  Wish = 2758726571
}

export enum FinalShapeEpisodeHashes {
  Echoes = 2758726572,
  Revenant = 2758726573,
  Heresy = 2758726574
}
