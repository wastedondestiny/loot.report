Time Wasted on Destiny
====================

## About
Time Wasted on Destiny is a website that tells you how much time you've spent on the Destiny series. On top of this, it is the only website that tells you just how much time you've spent on deleted characters, which you could consider wasted time.

**NOTE**: This page is only install instructions for the front-end of https://wastedondestiny.com. If you're looking for the API, please go to https://apiv3.wastedondestiny.com/docs.

## Installing
### Requirements
- Node 8.11+
- NPM 6.4+

Execute `npm install` to install dependencies, then run `npm run dev` to start a local development server.
To compile for production, run `npm run build`.

## Disclaimer
Time Wasted on Destiny is not affiliated with Bungie. Destiny is a registered trademark of Bungie.

## Licence
This project is distributed under the GPL v3.0 license. Please give credit if you intend on using it!
